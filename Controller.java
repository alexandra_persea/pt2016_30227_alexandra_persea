import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by alexandra on 29.02.2016.
 */
public class Controller {
    private View afisare;
    private Polinom p1;
    private Polinom p2;
    private Polinom p3 = new Polinom();

    public static Polinom getReal(String[] s) {
      Double[] val = new Double[s.length];
        for(int i = 0; i < s.length; i++){
            val[i] = Double.parseDouble(s[i]);
        }
        return new Polinom(val);
    }

    private ActionListener submit = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            for(int i = 0; i<afisare.getP1().length; i++) {
            if(afisare.getP1()[i].contains(".")) {
                p1 = Controller.getReal(afisare.getP1());
                break;
            }
        }
            if(p1 == null) {
                Integer[] val = new Integer[afisare.getP1().length];
                for (int i = 0; i < afisare.getP1().length; i++) {
                val[i] = Integer.valueOf(afisare.getP1()[i]);
            }
                p1 = new Polinom(val);
            }
        }
    };

    private ActionListener submit1 = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            for(int i = 0; i<afisare.getP2().length; i++) {
                if(afisare.getP2()[i].contains(".")) {
                    p2 = Controller.getReal(afisare.getP2());
                    break;
                }
            }
            if(p2 == null) {
                Integer[] val = new Integer[afisare.getP2().length];
                for (int i = 0; i < afisare.getP2().length; i++) {
                    val[i] = Integer.valueOf(afisare.getP2()[i]);
                }
                p2 = new Polinom(val);
            }
        }
    };

    private ActionListener adunare = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            if(p1 != null && p2 != null) {
                afisare.setSolution(Controller.this.add(p1, p2).toString());
                Controller.this.update();
            }
        }
    };

    private ActionListener substract = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (p1 != null && p2 != null) {
                afisare.setSolution(Controller.this.substract(p1, p2));
                Controller.this.update();
            }
        }
    };

    private ActionListener multiply = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            if(p1 != null && p2 != null) {
                afisare.setSolution(Controller.this.multiply(p1, p2));
                Controller.this.update();
            }

        }
    };

    private ActionListener divide = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            if(p1 != null && p2 != null) {
                afisare.setSolution(Controller.this.divide(p1, p2));
                Controller.this.update();
            }
        }
    };

    private ActionListener reset = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
           Controller.this.reset();

        }
    };

    public Controller(View x) {

        afisare = x;
        afisare.setp1submitListener(submit);
        afisare.setp2submitListener(submit1);
        afisare.addListener(adunare);
        afisare.setsubstractListener(substract);
        afisare.setMultiplyListener(multiply);
        afisare.setDivideListener(divide);
        afisare.setResetListener(reset);
    }

    public Controller() {

    }

    public String add(Polinom p1, Polinom p2) {
       p3 =  p1.adunare(p2);
       return this.afisareRez(p3);
    }

    public String substract(Polinom p1, Polinom p2) {
         p3 = p1.scadere(p2);
         return this.afisareRez(p3);
    }

    public String multiply(Polinom p1, Polinom p2) {
        Polinom p3 = p1.inmultire(p2);
//       Polinom p4 = p3.adunare(p3);
       return this.afisareRez(p3);
    }

    public void reset() {
        this.p1 = new Polinom();
        this.p2 = new Polinom();
        this.p3 = new Polinom();
    }

    public void update() {
        this.reset();
        for(int i = 0; i<afisare.getP1().length; i++) {
            if(afisare.getP1()[i].contains(".")) {
                p1 = Controller.getReal(afisare.getP1());
                break;
            }
        }
        if(p1.getMonom().size() == 0) {
            Integer[] val = new Integer[afisare.getP1().length];
            for (int i = 0; i < afisare.getP1().length; i++) {
                val[i] = Integer.valueOf(afisare.getP1()[i]);
            }
            p1 = new Polinom(val);
        }
        for(int i = 0; i<afisare.getP2().length; i++) {
            if(afisare.getP2()[i].contains(".") || p1.getMonom().get(i) instanceof MonomReal) {
                p2 = Controller.getReal(afisare.getP2());
                break;
            }
        }
        if(p2.getMonom().size() == 0) {
            Integer[] val = new Integer[afisare.getP2().length];
            for (int i = 0; i < afisare.getP2().length; i++) {
                val[i] = Integer.valueOf(afisare.getP2()[i]);
            }
            p2 = new Polinom(val);
        }
        p3 = new Polinom();
    }
    public String divide(Polinom p1, Polinom p2) {
        Polinom p3 = p1.impartire(p2);
        return this.afisareRez(p3);
    }

    public String afisareRez(Polinom p1){
        String s = new String();
        for(int i = 0; i<p1.getMonom().size(); i++) {
            if(i < p1.getMonom().size()-1)
            s = s.concat(p1.getMonom().get(i).getCoeficient() + "X^" + p1.getMonom().get(i).getGrad() + " + ");
            else
               s = s.concat(p1.getMonom().get(i).getCoeficient() + "X^" + p1.getMonom().get(i).getGrad());
        }
        if(p1.getRest().size() > 0 ){
            System.out.print(" R: ");
            for(int i = 0; i<p1.getRest().size(); i++) {
                if (i < p1.getRest().size() - 1)
                   s = s.concat(p1.getRest().get(i).getCoeficient() + "X^" + p1.getRest().get(i).getGrad() + " + ");
                else
                   s = s.concat(p1.getRest().get(i).getCoeficient() + "X^" + p1.getRest().get(i).getGrad());
            }
        }

        if(s.length() == 0) {
            s = "0x^0";
        }
        return s;
    }
}
