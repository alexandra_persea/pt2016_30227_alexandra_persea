

/**
 * Created by alexandra on 05.03.2016.
 */
public interface Monom {

    public abstract Monom aduna(Monom x);
    public abstract Monom scade(Monom x);
    public abstract Monom inmultire(Monom x);
    public abstract Monom impartire(Monom x);
    public abstract int getGrad();
    public abstract Number getCoeficient();
}
