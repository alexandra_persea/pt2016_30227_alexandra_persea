/**
 * Created by alexandra on 29.02.2016.
 */
public class MonomInt implements Monom {
    private int coeficient;
    private int grad;

    public MonomInt(int x, int y) {
        grad = x;
        coeficient = y;
    }

    public MonomInt aduna(Monom x){
        return new MonomInt(grad, coeficient + (int)x.getCoeficient());
    }

    public MonomInt scade(Monom x) {
       return new MonomInt(grad, coeficient - (int)x.getCoeficient());
    }

    public MonomInt inmultire(Monom x) {
        if (x.getCoeficient() instanceof Double) {
            return new MonomInt(this.getGrad() + x.getGrad(), this.getCoeficient() * x.getCoeficient().intValue());
        } else {
            return new MonomInt(this.getGrad() + x.getGrad(), this.getCoeficient() * (int)x.getCoeficient());
        }
    }

    public MonomReal impartire(Monom x) {
            return new MonomReal(this.getGrad() - x.getGrad(), this.getCoeficient() / Double.valueOf((double)(int)x.getCoeficient()));
    }

    public int getGrad() {
        return this.grad;
    }

    public Integer getCoeficient() {
        return ((int)this.coeficient);
    }
}
