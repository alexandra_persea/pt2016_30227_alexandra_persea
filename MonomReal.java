/**
 * Created by alexandra on 29.02.2016.
 */
public class MonomReal implements Monom {
    private Double coeficient;
    private int grad;

    public MonomReal(int x, double y) {
        grad = x;
        coeficient = y;
    }

    public MonomReal aduna(Monom x){
        return new MonomReal(grad, coeficient + (double)x.getCoeficient());
    }

    public MonomReal scade(Monom x) {
        return new MonomReal(grad, coeficient - (double)x.getCoeficient());
    }

    public MonomReal inmultire(Monom x) {
        if(x instanceof MonomReal) {
            return new MonomReal(this.getGrad() + x.getGrad(), this.getCoeficient() * (Double)x.getCoeficient());
        }
        else {
            return new MonomReal(this.getGrad() + x.getGrad(), this.getCoeficient() * Double.valueOf((double) x.getCoeficient().intValue()));
        }
    }

    public MonomReal impartire(Monom x) {
        return new MonomReal(this.getGrad() - x.getGrad(), this.getCoeficient() / Double.valueOf((double) x.getCoeficient()));
    }

    public int getGrad() {
        return this.grad;
    }

    public Double getCoeficient() {
        return (this.coeficient);
    }
}
