import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by alexandra on 29.02.2016.
 */
public class Polinom {
    private int grad;

    private ArrayList<Monom> monom = new ArrayList<>();
    private ArrayList<Monom> rest = new ArrayList<>();

    public Polinom(Integer y[]) {
        this.grad = y.length-1;
       int x = this.grad;
        for(int i = 0; i<= this.grad; i++) {
            monom.add(new MonomInt(x, y[i]));
            x--;
        }
    }

    public Polinom(Double y[]) {
        this.grad = y.length-1;
        int x = this.grad;
        for(int i = 0; i<= this.grad; i++) {
            monom.add(new MonomReal(x, y[i]));
            x--;
        }
    }

    public Polinom(int grad) {
        this.grad = grad;
    }

    public Polinom(Monom m) {
        monom.add(m);
    }

    public Polinom() {

    }

    public ArrayList<Monom> getRest() {
        return this.rest;
    }

    public Polinom adunare(Polinom p2) {
        Polinom p3 = new Polinom();
        for(int i = 0; i < monom.size(); i++) {
            for (int j = 0; j < p2.getMonom().size(); j++) {
                if (i>=0 && j>=0 && monom.get(i).getGrad() == p2.monom.get(j).getGrad()) {
                    p3.getMonom().add(monom.get(i).aduna(p2.monom.get(j)));
                    monom.remove(i);
                    p2.getMonom().remove(j);
                    j--;
                    i--;
                }
            }
        }
        if(monom.size() > 0) {
            p3.getMonom().addAll(monom);
        }
        if(p2.getMonom().size() > 0) {
            p3.getMonom().addAll(p2.getMonom());
        }
        p3.sort();
        return p3;
    }

    public Polinom scadere(Polinom p2) {
        Polinom p3 = new Polinom();
        for(int i = 0; i < monom.size(); i++) {
            for (int j = 0; j < p2.getMonom().size(); j++) {
                if (i >= 0 && j >= 0 && monom.get(i).getGrad() == p2.monom.get(j).getGrad()) {
                    p3.getMonom().add(monom.get(i).scade(p2.monom.get(j)));
                    monom.remove(i);
                    p2.getMonom().remove(j);
                    j--;
                    i--;
                }
            }
        }
        if(monom.size() > 0) {
            p3.getMonom().addAll(monom);
        }
        if(p2.getMonom().size() > 0) {
            p3.getMonom().addAll(p2.getMonom());
        }
        p3.sort();
        return p3;
    }

    public void sort() {
        this.update();
        for(int i = 1; i < this.getMonom().size(); i++) {
                int j = i - 1;
            while (j >= 0 && this.getMonom().get(i).getGrad() > this.getMonom().get(j).getGrad()) {
                    Collections.swap(this.getMonom(), i, j);
                    j--;
                    i--;
                }
        }
    }

    public void update() {
        for (int i = 0; i< this.getMonom().size(); i++) {
            if(this.getMonom().get(i) instanceof MonomReal) {
                if((Double)this.getMonom().get(i).getCoeficient() == 0) {
                    this.getMonom().remove(i);
                    i--;
                    this.grad--;
                }
            }
            else
                if((Integer)this.getMonom().get(i).getCoeficient() == 0) {
                    this.getMonom().remove(i);
                    i--;
                    this.grad--;
                }

        }
        for(int i = 0; i<this.getMonom().size(); i++) {
            if(this.getMonom().get(i).getGrad() > this.grad) {
                this.grad = this.getMonom().get(i).getGrad();
            }
        }

    }

    public Polinom inmultire(Polinom p2) {
        Polinom p3 = new Polinom();
            for (int i = 0; i < this.getMonom().size(); i++) {
                for (int j = 0; j < p2.getMonom().size(); j++) {
                    if (monom.get(i) instanceof MonomReal) {
                        p3.monom.add(((MonomReal)monom.get(i)).inmultire(p2.getMonom().get(j)));
                    } else {
                        p3.monom.add(monom.get(i).inmultire(p2.getMonom().get(j)));
                    }
                }
            }
        for(int i = 0; i<p3.getMonom().size(); i++) {
            for(int j = 0; j<p3.getMonom().size(); j++) {
                if (i != j && i>=0 && j>=0 && p3.getMonom().get(i).getGrad() == p3.getMonom().get(j).getGrad()) {
                    p3.getMonom().add(i, p3.getMonom().get(i).aduna(p3.getMonom().get(j)));
                    p3.getMonom().remove(i+1);
                    p3.getMonom().remove(j);
                    i--;
                    j--;
                }
            }
        }
        p3.sort();
        return p3;
    }

    public void toReal(){
        Polinom p3 = new Polinom();
        for(int i = 0; i<this.getMonom().size(); i++) {
            if(this.getMonom().get(i).getCoeficient() instanceof Double) {
                MonomReal m = new MonomReal(this.getMonom().get(i).getGrad(), Double.valueOf((double)this.getMonom().get(i).getCoeficient()));
                p3.getMonom().add(m);
            }
            else {
                MonomReal m = new MonomReal(this.getMonom().get(i).getGrad(), Double.valueOf((double)(int)this.getMonom().get(i).getCoeficient()));
                p3.getMonom().add(m);
            }
        }
        this.getMonom().removeAll(this.getMonom());
        this.getMonom().addAll(p3.getMonom());
    }

    public Polinom impartire(Polinom p2) {
        this.toReal();
        p2.toReal();
        Polinom p3 = new Polinom();
        this.sort();
        p2.sort();
        while (this.getGrad() >= p2.getGrad() && this.getMonom().size() > 0 && p2.getMonom().size() >= 0) {
          //  if(this.getGrad() >= p2.getGrad()) {
                Monom m = (MonomReal)this.getMonom().get(0).impartire(p2.getMonom().get(0));
                p3.getMonom().add(m);
             //   this.getMonom().removeAll(this.getMonom());
                Integer coef[] = {-1};
                Polinom temp = new Polinom(m);
                temp = p2.inmultire(temp);
                temp = temp.inmultire((new Polinom(coef)));
             //   temp.getMonom().addAll(temp.getMonom());
                Polinom pc = this.adunare(temp);
                this.monom.removeAll(this.getMonom());
                this.monom.addAll(pc.getMonom());
                this.update();
       // }
        }
    if(this.getMonom().size() > 0){
        p3.getRest().addAll(this.getMonom());
    }
        return p3;
    }

    public int getGrad() {
        return grad;
    }

    public ArrayList<Monom> getMonom() {
        return this.monom;
    }
}
