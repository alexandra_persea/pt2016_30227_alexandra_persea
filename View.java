import oracle.jrockit.jfr.JFR;

import javax.swing.*;
import javax.swing.text.AbstractDocument;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Arc2D;

/**
 * Created by alexandra on 29.02.2016.
 */
public class View extends JFrame {

  private  String i;
  private  String j;

  private  Integer grad;
  private  Integer[] val;
    private Double[] doubleVal;
    private JPanel content;
    private JPanel principal;
    private JButton adunare;
    private JButton substraction;
    private JButton multiply;
    private JButton divide;
    private JButton submit;
    private JButton submit1;
    private JTextField p1;
    private JTextField p2;
    private JTextField solution;
    private JButton reset;
    private JLabel l1;
    // for (int i = 0; i < (int)grad; i++)
    public View() {
        this.setSize(458, 345);
        this.setLayout(new BorderLayout());
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        p1 = new JTextField(30);
        p2 = new JTextField(30);
        solution = new JTextField(30);
        content = new JPanel();
        submit = new JButton("submit");
        submit1 = new JButton("submit");
        reset = new JButton("reset");
        l1 = new JLabel();
        l1.setText("<html><br><h1>Insert first polynomial</h1></br></html>");
        content.add(l1);
        content.add(p1);
        content.add(submit);
         JLabel l2 = new JLabel();
        JLabel l3 = new JLabel();
        l2.setText("<html><br><h1>Insert second polynomial</h1><br></html>");
        content.add(l2);
        content.add(p2);
        content.add(submit1);
        adunare = new JButton("add");
        substraction = new JButton("substract");
        multiply = new JButton("multiply");
        divide = new JButton("divide");
        l3.setText("<html><p>Operations:</br><br></p></html>");
        content.add(l3);
        content.add(adunare);
        content.add(substraction);
        content.add(multiply);
        content.add(divide);
        content.add(solution);
        content.add(reset);
        this.add(content);

        this.setVisible(true);
//        this.i = JOptionPane.showInputDialog(this, "Introduceti gradul polinomului");
//
//        this.j = JOptionPane.showInputDialog(this, "Introduceti coeficientii separati prin spatiu", "");
//        content = new JPanel();
//        principal = new JPanel();
//        content.add(principal);
//        adunare = new JButton();
//        principal.add(adunare);
//        this.setContentPane(content);
//        this.setSize(300, 300);
    }

    private void actualizare() {
        String[] s = j.split(" ");
        val = new Integer[grad];
        for(int i = 0; i < s.length -1 ; i++) {
            val[i] = Integer.valueOf(s[i]);
        }
    }

//    public Double[] getReal(String[] s) {
//      Double[] val = new Double[s.length];
//        for(int i = 0; i < s.length; i++){
//            val[i] = Double.parseDouble(s[i]);
//        }
//        return val;
//    }
//    public Number[] getP1() {
//        String[] s = p1.getText().split(" ");
//        for(int i = 0; i<s.length; i++) {
//            if(s[i].contains(".")) {
//                doubleVal = this.getReal(s);
//                return doubleVal;
//            }
//        }
//        if(doubleVal.length == 0) {
//            for (int i = 0; i < s.length; i++) {
//                val[i] = Integer.valueOf(s[i]);
//            }
//        }
//
//        return val;
//    }
//

    public String[] getP1(){

        return p1.getText().split(" ");
    }
    public String[] getP2() {
        return p2.getText().split(" ");
    }

    public void setSolution(String s) {

        solution.setText(s);
    }

    public void setp1submitListener(ActionListener listenp1) {
        submit.addActionListener(listenp1);
    }

    public void setp2submitListener(ActionListener listenp2) {
        submit1.addActionListener(listenp2);
    }

    public void addListener(ActionListener listenadd) {
        adunare.addActionListener(listenadd);
    }

    public void setsubstractListener(ActionListener listensub) {
        substraction.addActionListener(listensub);
    }

    public void setMultiplyListener(ActionListener listemul) {
        multiply.addActionListener(listemul);
    }

    public void setDivideListener(ActionListener listendiv) {
        divide.addActionListener(listendiv);
    }

    public void setResetListener(ActionListener listenReset) {
        reset.addActionListener(listenReset);
    }
    public void displayErrorMessage(String er) {
        JOptionPane.showMessageDialog(this, er);
    }

    public JButton getAdunare() {
        return adunare;
    }

    public Integer getGrad() {
        return grad;
    }

    public Integer[] getVal() {
        return val;
    }
}
